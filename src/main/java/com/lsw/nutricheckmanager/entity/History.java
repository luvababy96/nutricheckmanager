package com.lsw.nutricheckmanager.entity;

import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import com.lsw.nutricheckmanager.model.HistoryItem;
import com.lsw.nutricheckmanager.model.HistoryRequest;
import com.lsw.nutricheckmanager.model.PillUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class History {

    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "영양제 id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pillId", nullable = false)
    private Pill pill;

    @ApiModelProperty(notes = "복용 일자")
    @Column(nullable = false)
    private LocalDate eatDate;

    @ApiModelProperty(notes = "복용 시간")
    @Column(nullable = false)
    private LocalTime eatTime;

    @ApiModelProperty(notes = "메모")
    @Column(length = 100)
    private String memo;


    private History(HistoryBuilder builder) {
        this.pill = builder.pill;
        this.eatDate = builder.eatDate;
        this.eatTime = builder.eatTime;
        this.memo = builder.memo;
    }

    public static class HistoryBuilder implements CommonModelBuilder<History> {
        private final Pill pill;
        private final LocalDate eatDate;
        private final LocalTime eatTime;
        private String memo; // 필수로 값 아니라 final 생략

        public HistoryBuilder(Pill pill, HistoryRequest historyRequest) {
            this.pill = pill;
            this.eatDate = historyRequest.getEatDate();
            this.eatTime = LocalTime.of(historyRequest.getEatTimeHour(),historyRequest.getEatTimeMinute()); //시,분만 입력할 수 있도록 of를 사용하여 각각 받아 정리한다.
            // memo는 따로 구성해준다.(필수값 아니기 때문)
        }

        // 새로운 직원(setMemo)가 어떤 형태(string)의 이름(memo)로 가져올지 입력해주기
        public HistoryBuilder setMemo(String memo) {
            this.memo = memo;

            return this;
        }

        @Override
        public History build() {
            return new History(this);
        }
    }



}
