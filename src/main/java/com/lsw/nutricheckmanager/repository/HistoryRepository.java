package com.lsw.nutricheckmanager.repository;

import com.lsw.nutricheckmanager.entity.History;
import com.lsw.nutricheckmanager.entity.Pill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Long> {

    //꼭 물어보기!!! 왜 !!!
    long countByPillIdAndEatDate(long pillID, LocalDate eatDate);

    List<History> findAllByEatDate(LocalDate eatDate);

}
