package com.lsw.nutricheckmanager.repository;

import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.model.PillRepurchaseItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PillRepository extends JpaRepository<Pill, Long> {

    List<Pill> findAllByIsDailyAndIsEatOrderByPillNameAsc(boolean isDaily,boolean isEat);

    List<Pill> findAllByPillNameLikeOrderByPillNameAsc(String searchName);

    List<Pill> findAllByRemainingQuantityLessThanEqualAndIsEatOrderByPillNameAsc(int quantity, boolean isEat);
}
