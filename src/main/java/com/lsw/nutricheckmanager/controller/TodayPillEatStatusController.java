package com.lsw.nutricheckmanager.controller;

import com.lsw.nutricheckmanager.model.CommonResult;
import com.lsw.nutricheckmanager.model.HistoryRequest;
import com.lsw.nutricheckmanager.model.ListResult;
import com.lsw.nutricheckmanager.model.TodayPillEatStatusItem;
import com.lsw.nutricheckmanager.service.NutriEatService;
import com.lsw.nutricheckmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "오늘의 복용 현황")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/nutri-status")
public class TodayPillEatStatusController {

    private final NutriEatService nutriEatService;

    @ApiOperation(value = "복용 현황 목록")
    @GetMapping("/today")
    public ListResult<TodayPillEatStatusItem> getTodayStatus() {
        return ResponseService.getListResult(nutriEatService.getTodayStatus(),true);
        // isSuccess : 모든 결과가 성공해야 true값을 주는건데 controller까지 왔으면 일단 무조건 true. 근데 이해하려고 하지 X 이해 불가함.

    }

    @ApiOperation(value = "복용 내역 등록")
    @PostMapping("/do-eat/pill-id/{}pillId")
    public CommonResult setDoEat(@PathVariable long pillId, @RequestBody @Valid HistoryRequest request) {
        nutriEatService.doEat(pillId, request);
        return ResponseService.getSuccessResult();

    }


}
