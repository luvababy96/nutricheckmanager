package com.lsw.nutricheckmanager.controller;

import com.lsw.nutricheckmanager.model.*;
import com.lsw.nutricheckmanager.service.PillService;
import com.lsw.nutricheckmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Api(tags = "영양제 목록")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pill")
public class PillController {

    private final PillService pillService;

    @ApiOperation(value = "영양제 등록")
    @PostMapping("/data")
    public CommonResult setPill(@RequestBody @Valid PillRequest request) {
        pillService.setPill(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "영양제 이름으로 검색")
    @GetMapping("/search")
    public ListResult<PillItem> getPillsBySearchName(@RequestParam("searchName") String searchName) {

        // ResponseService에 getListResult 에서 isSuccess를 true나 false로 수기입력 받는 이유는
        // 예를들면 재가공이 무조건 성공이 아니라 실패하는 경우도 있다고 치자.
        // 이런경우 모두 성공이 아니기 때문에 요청 자체가 성공이라고 판단하기 어렵다.
        // 5개를 가공해서 보여줄려고 했는데 3개는 성공하고 2개는 실패했을 때 이건 사실 성공이 아니라 2개의 실패에 초점을 맞춰 실패로 보는게 맞다.
        // 이런 상황을 대비해서 isSuccess 에 true나 false를 수기로 기입하게끔 해놓았는데
        // 현재 상황은 무조건 재가공이 성공하는 상황이기 때문에 true가 들어간다.
        return ResponseService.getListResult(pillService.getPillsByLikeName(searchName),true);
    }

    @ApiOperation(value = "재구매 임박 리스트")
    @GetMapping("/repurchase")
    public ListResult<PillRepurchaseItem> getRepurchase() {
        return ResponseService.getListResult(pillService.getRepurchase(),true);
    }
    @ApiOperation(value = "영양제 내역 목록")
    @GetMapping("/all")
    public ListResult<PillItem> getPills() {
        return ResponseService.getListResult(pillService.getPills(),true);
    }

    @ApiOperation(value = "영양제 상세 내역")
    @GetMapping("/detail/{id}")
    public SingleResult<PillResponse> getPill(@PathVariable long id) {
        return ResponseService.getSingleResult(pillService.getPill(id));
    }

    @ApiOperation(value = "영양제 정보 수정")
    @PutMapping("/info/{id}")
    public CommonResult putPillInfo(@PathVariable long id, @RequestBody @Valid PillUpdateRequest request) {
        pillService.putPillInfo(id,request);

        return ResponseService.getSuccessResult();

    }


}
