package com.lsw.nutricheckmanager.controller;

import com.lsw.nutricheckmanager.model.HistoryItem;
import com.lsw.nutricheckmanager.model.ListResult;
import com.lsw.nutricheckmanager.service.HistoryService;
import com.lsw.nutricheckmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "복용 내역")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {

    private final HistoryService historyService;

    @ApiOperation(value = "복용 내역 목록")
    @GetMapping("/all")
    public ListResult<HistoryItem> getHistories() {
        return ResponseService.getListResult(historyService.getHistories(),true);
    }
}
