package com.lsw.nutricheckmanager.service;

import com.lsw.nutricheckmanager.entity.History;
import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.exception.CEatDateFutureException;
import com.lsw.nutricheckmanager.exception.CExcessEatException;
import com.lsw.nutricheckmanager.exception.CMissingDataException;
import com.lsw.nutricheckmanager.model.HistoryRequest;
import com.lsw.nutricheckmanager.model.ListResult;
import com.lsw.nutricheckmanager.model.TodayPillEatStatusItem;
import com.lsw.nutricheckmanager.repository.HistoryRepository;
import com.lsw.nutricheckmanager.repository.PillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NutriEatService {
    private final PillRepository pillRepository;
    private final HistoryRepository historyRepository;

    public ListResult<TodayPillEatStatusItem> getTodayStatus() {
        List<TodayPillEatStatusItem> result = new LinkedList<>();

        List<Pill> pillList = pillRepository.findAllByIsDailyAndIsEatOrderByPillNameAsc(true,true);
        List<History> historyList = historyRepository.findAllByEatDate(LocalDate.now());

        for (Pill pillItem : pillList) {

            // 이름 (pillFullName은 model에서 생성한 이름과 동일하게 작성 (내가 안헷갈리기 위해)
            String pillFullName = pillItem.getPillCompany() + " " + pillItem.getPillName();
            int dosesCount = pillItem.getOneTimeQuantity();
            int realDosesCount = 0; // 실 복용 수의 기본 값은 0이니까

            for (History historyItem  : historyList) {
                if(historyItem.getPill().getId().equals(pillItem.getId())) {
                    realDosesCount += 1; // 굳이 한 줄에 {}을 쓴건 가독성을 위해(고단수는 () 써버리면 더 베스트!)
                }
            }
            result.add(new TodayPillEatStatusItem.TodayPillEatStatusItemBuilder(
                    pillFullName, // 위에 적힌 string 형태의 pillFullName을 뜻 함.(builder에서 작성해놓은 이름과는 무관/class내 에서만 이름 사용 가능)
                    dosesCount,
                    realDosesCount).build());
        }

        return ListConvertService.settingResult(result);


    }

    public void doEat(long pillId, HistoryRequest historyRequest) {
        // 영양제 데이터를 가져온다.(영양제 데이터가 없으면 없는 영양제를 먹을 수 없으니 error)
        Pill pill = pillRepository.findById(pillId).orElseThrow(CMissingDataException::new);



        long todayEat = historyRepository.countByPillIdAndEatDate(pill.getId(), historyRequest.getEatDate());
        LocalDate today = LocalDate.now();

        if(today.isBefore(historyRequest.getEatDate())) throw new CEatDateFutureException();
        if(pill.getOneTimeQuantity() <= todayEat) throw new CExcessEatException();



        // 복용 내역 먼저 등록(실패 시 잔여수량 깎이면 안되므로 복용내역부터 실행)
        History history = new History.HistoryBuilder(pill,historyRequest).setMemo(historyRequest.getMemo()).build();
        historyRepository.save(history);

        // 복용내역 등록 후 잔여갯수에서 1회당 복용 수 차감 실행
        pill.putRemainCount();
        // 수정된 영양제 데이터 재 저장
        pillRepository.save(pill);
    }
}
