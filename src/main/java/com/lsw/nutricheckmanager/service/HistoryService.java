package com.lsw.nutricheckmanager.service;

import com.lsw.nutricheckmanager.entity.History;
import com.lsw.nutricheckmanager.model.HistoryItem;
import com.lsw.nutricheckmanager.model.ListResult;
import com.lsw.nutricheckmanager.repository.HistoryRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {

    private final HistoryRepository historyRepository;

    public ListResult<HistoryItem> getHistories() {
        List<History> originList = historyRepository.findAll();

        List<HistoryItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HistoryItem.HistoryItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
