package com.lsw.nutricheckmanager.service;

import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.exception.CDataPastException;
import com.lsw.nutricheckmanager.exception.CMissingDataException;
import com.lsw.nutricheckmanager.model.*;
import com.lsw.nutricheckmanager.repository.PillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PillService {

    private final PillRepository pillRepository;

    public void setPill(PillRequest request) {
        LocalDate today = LocalDate.now(); // today 변수 만들기

        //오늘이 받을 값보다 나중(after)이면 true
        if(today.isAfter(request.getExpiationDate())) throw new CDataPastException();
        Pill addData = new Pill.PillBuilder(request).build();

        pillRepository.save(addData);
    }

    public PillResponse getPill(long id) {
        Pill originData = pillRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new PillResponse.PillResponseBuilder(originData).build();
    }
    public ListResult<PillItem> getPills() {
        List<Pill> originList = pillRepository.findAll();

        List<PillItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new PillItem.PillItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<PillItem> getPillsByLikeName(String searchName) {
        List<Pill> originList = pillRepository.findAllByPillNameLikeOrderByPillNameAsc("%" + searchName + "%");

        List<PillItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new PillItem.PillItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<PillRepurchaseItem> getRepurchase() {
        List<PillRepurchaseItem> result = new LinkedList<>();

        List<Pill> originList = pillRepository.findAllByRemainingQuantityLessThanEqualAndIsEatOrderByPillNameAsc(15,true);

        originList.forEach(item -> result.add(new PillRepurchaseItem.PillRepurchaseItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putPillInfo(long id,PillUpdateRequest request) {
        Pill originData = pillRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putPillInfo(request);

        pillRepository.save(originData);
    }


}
