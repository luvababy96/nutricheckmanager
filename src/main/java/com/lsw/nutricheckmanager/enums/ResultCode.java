package com.lsw.nutricheckmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다.")

    , DATE_PAST(-20000, "유통기한이 지난 제품입니다.")

    , EXCESS_EAT(-30000, "일일 복용횟수를 초과했습니다.")
    , EAT_DATE_FUTURE(-30001, "복용일자는 오늘 이후 날짜를 선택할 수 없습니다.")

    ;

    private final Integer code;
    private final String msg;
}
