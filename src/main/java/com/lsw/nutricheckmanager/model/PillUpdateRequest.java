package com.lsw.nutricheckmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PillUpdateRequest {

    @ApiModelProperty(notes = "섭취 주기 여부", required = true)
    @NotNull
    private  Boolean isDaily;

    @ApiModelProperty(notes = "1회 복용 횟수 (1 이상)", required = true)
    @NotNull
    @Min(value = 1)
    private  Integer oneTimeQuantity;

}
