package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.enums.NutriInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PillRequest {

    @ApiModelProperty(notes = "영양제 이름 (1~35자)", required = true)
    @NotNull
    @Length(min = 1, max = 35)
    private  String pillName;

    @ApiModelProperty(notes = "영양제 제조사 (1~20 자)", required = true)
    @NotNull
    @Length(min = 1, max = 20) // string은 길이조절을 해야해서 min,max lenght를 이용해서 사용
    private  String pillCompany;

    @ApiModelProperty(notes = "영양제 가격 (0 이상)", required = true)
    @NotNull
    @Min(value = 0) // integer은 값 자체의 숫자의 범위를 지정하는거라 바로 min,max씀
    private  Integer price;

    @ApiModelProperty(notes = "영양 정보", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private  NutriInfo nutriInfo;

    @ApiModelProperty(notes = "영양제 복용시작일자 (yyyy-mm-dd)", required = true)
    @NotNull
    private  LocalDate startDate;

    @ApiModelProperty(notes = "영양제 유통기한 (yyyy-mm-dd)", required = true)
    @NotNull
    private  LocalDate expiationDate;

    @ApiModelProperty(notes = "영양제 총 수량(1회 기준) (1이상)", required = true)
    @NotNull
    @Min(value = 1)
    private Integer totalQuantity;

    @ApiModelProperty(notes = "섭취 주기 여부", required = true)
    @NotNull
    private  Boolean isDaily;

    @ApiModelProperty(notes = "1회 복용 횟수 (1 이상)", required = true)
    @NotNull
    @Min(value = 1)
    private  Integer oneTimeQuantity;

}
