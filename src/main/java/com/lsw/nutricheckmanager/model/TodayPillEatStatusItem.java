package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TodayPillEatStatusItem {

    //기존 자료를 토대로 가지고 오는 것이므로 valid 필요 없음
    @ApiModelProperty(notes = "영양제 이름 및 제조사")
    private String pillFullName;

    @ApiModelProperty(notes = "권장 복용량")
    private Integer dosesCount;

    @ApiModelProperty(notes = "실제 복용량")
    private Integer realDosesCount;

    private TodayPillEatStatusItem(TodayPillEatStatusItemBuilder builder) {
        this.pillFullName = builder.pillFullName;
        this.dosesCount = builder.dosesCount;
        this.realDosesCount = builder.realDosesCount;

    }

    public static class TodayPillEatStatusItemBuilder implements CommonModelBuilder<TodayPillEatStatusItem> {
        private final String pillFullName;

        private final Integer dosesCount;

        private final Integer realDosesCount;

        public TodayPillEatStatusItemBuilder( //바구니 안에는 어떤 형태를 가져다 쓸건지가 중요.(builder는 값을 가져오는 것)
                String pillFullName,
                Integer dosesCount,
                Integer realDosesCount
        )
        {
            this.pillFullName = pillFullName;
            this.dosesCount = dosesCount;
            this.realDosesCount = realDosesCount;

        }

        @Override
        public TodayPillEatStatusItem build() {
            return new TodayPillEatStatusItem(this);
        }
    }




}
