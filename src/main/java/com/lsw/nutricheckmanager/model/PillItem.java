package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillItem {

    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "영양제 이름")
    private String pillName;
    @ApiModelProperty(notes = "영양제 제조사")
    private String pillCompany;
    @ApiModelProperty(notes = "영양제 가격")
    private Integer pillPrice;
    @ApiModelProperty(notes = "현재 복용 여부")
    private String isEat;

    private PillItem(PillItemBuilder builder){
        this.id = builder.id;
        this.pillName = builder.pillName;
        this.pillCompany = builder.pillCompany;
        this.pillPrice = builder.pillPrice;
        this.isEat = builder.isEat;
    }

    public static class PillItemBuilder implements CommonModelBuilder<PillItem> {

        private final Long id;
        private final String pillName;
        private final String pillCompany;
        private final Integer pillPrice;
        private final String isEat;

        public PillItemBuilder(Pill pill) {
            this.id = pill.getId();
            this.pillName = pill.getPillName();
            this.pillCompany = pill.getPillCompany();
            this.pillPrice = pill.getPrice();
            this.isEat = pill.getIsEat()? "O" : "X";
        }

        @Override
        public PillItem build() {
            return new PillItem(this);
        }
    }


}
