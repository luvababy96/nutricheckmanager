package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.enums.NutriInfo;
import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillRepurchaseItem {

    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "영양제 이름 및 제조사")
    private String pillFullName;
    @ApiModelProperty(notes = "영양제 가격")
    private Integer pillPrice;
    @ApiModelProperty(notes = "영양 정보 이름")
    private String nutriInfoName;
    @ApiModelProperty(notes = "잔여 수량")
    private Integer remainingQuantity;


    private PillRepurchaseItem(PillRepurchaseItemBuilder builder) {
        this.id = builder.id;
        this.pillFullName = builder.pillFullName;
        this.pillPrice = builder.pillPrice;
        this.nutriInfoName = builder.nutriInfoName;
        this.remainingQuantity = builder.remainingQuantity;
    }

    public static class PillRepurchaseItemBuilder implements CommonModelBuilder<PillRepurchaseItem> {

        private final Long id;
        private final String pillFullName;
        private final Integer pillPrice;
        private final String nutriInfoName;
        private final Integer remainingQuantity;

        public PillRepurchaseItemBuilder(Pill pill) {
            this.id = pill.getId();
            this.pillFullName = pill.getPillName() + " " + pill.getPillCompany();
            this.pillPrice = pill.getPrice();
            this.nutriInfoName = pill.getNutriInfo().getName();
            this.remainingQuantity = pill.getRemainingQuantity();
        }

        @Override
        public PillRepurchaseItem build() {
            return new PillRepurchaseItem(this);
        }
    }


}
